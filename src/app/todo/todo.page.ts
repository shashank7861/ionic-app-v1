import { Component, OnInit } from '@angular/core';
import { Todo } from './todo.model';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
})
export class TodoPage implements OnInit {

  todo: Todo[];

  constructor(private ts: TodoService) { }
  ngOnInit() {
    this.todo = this.ts.getAlltodo();
  }

}
