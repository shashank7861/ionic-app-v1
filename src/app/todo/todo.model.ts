export interface Todo{
    id: string;
    title: string;
    content: string;
    image: string;
    date: string;
    status: string;
}