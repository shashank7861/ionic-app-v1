import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { StorageService } from '../services/storage.service';
import { Todo } from '../todo/todo.model';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.page.html',
  styleUrls: ['./addtask.page.scss'],
})
export class AddtaskPage implements OnInit {

  public tasks: Todo[];
  addtaskForm: FormGroup;
  myDate = new Date().toISOString();
  constructor(
    public formbuilder: FormBuilder,
    private ss: StorageService
    ) {
    this.addtaskForm = this.formbuilder.group({
      id: new FormControl('1'),
      title: new FormControl('', Validators.compose([Validators.required])),
      content: new FormControl('', Validators.compose([Validators.required])),
      date: new FormControl(this.myDate, Validators.compose([Validators.required])),
      status: new FormControl('i', Validators.compose([Validators.required])),
      image: new FormControl('i')
    });
   }

  ngOnInit() {
    this.tasks = this.ss.getTodo();
    console.log(this.tasks);
  }
  addTask() {
    console.log(this.addtaskForm.value);
    return this.ss.addTodo(this.addtaskForm.value);
  }
  
}
