import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'todo', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'todo',
    children: [
      {
        path: '',
        loadChildren: () => import('./todo/todo.module').then( m => m.TodoPageModule),
      },
      {
        path: ':id',
        loadChildren: () => import('./task/task.module').then( m => m.TaskPageModule)
      }
    ]
  },
  {
    path: 'addtask',
    loadChildren: () => import('./addtask/addtask.module').then( m => m.AddtaskPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
