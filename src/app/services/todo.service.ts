import { Injectable } from '@angular/core';
import { Todo } from '../todo/todo.model';
@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todo: Todo[] = [
    {
      id: 'task1',
      title: 'API',
      content: 'Create API to use it in app as well as web application.',
      date: '12/12/2019',
      image: 'https://webme.ie/wp-content/uploads/2019/08/How-to-run-a-python-django-app-in-docker.png',
      status: 'i',
    },
    {
      id: 'task2',
      title: 'API',
      content: 'Create API to use it in app as well as web application.',
      date: '12/12/2019',
      image: 'https://webme.ie/wp-content/uploads/2019/08/How-to-run-a-python-django-app-in-docker.png',
      status: 'i',
    },
    {
      id: 'task3',
      title: 'API',
      content: 'Create API to use it in app as well as web application.',
      date: '12/12/2019',
      image: 'https://webme.ie/wp-content/uploads/2019/08/How-to-run-a-python-django-app-in-docker.png',
      status: 'i',
    }
  ];
  constructor() { }

  getAlltodo() {
    return [...this.todo];
  }

  gettask(id: string) {
    return {...this.todo.find(todo => {
      return todo.id === id;
    })
    };
  }

  deleteTask(id: string) {
    console.log('deleting');
    this.todo = this.todo.filter(todo => {
      return todo.id !== id;
    });
    console.log(this.todo);
  }

}
