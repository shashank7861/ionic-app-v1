import { Injectable } from '@angular/core';
import { Todo } from '../todo/todo.model';
import { Storage } from '@ionic/storage';
const TODO_KEY = 'my-todo';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }

  addTodo(task: Todo) {
    return this.storage.get(TODO_KEY).then((tasks: Todo[]) => {
      if (tasks) {
        tasks.push(task);
        return this.storage.set(TODO_KEY, [tasks]);
      } else {
        return this.storage.set(TODO_KEY, [task]);
      }
    });
  }
  getTodo(): Promise<Todo[]> {
    return this.storage.get(TODO_KEY);
  }
  updateTodo(task: Todo) {
    return this.storage.get(TODO_KEY).then((tasks: Todo[]) => {
      if (!tasks || HTMLIonItemSlidingElement.length === 0) {
        return null;
      }
      const newTasks: Todo[] = [];
      for (const i of tasks) {
        if (i.id === task.id) {
          newTasks.push(task);
        } else {
          newTasks.push(i);
        }
      }
      return this.storage.set(TODO_KEY, newTasks);
    });
  }
  deleteTodo(id): Promise<Todo> {
    return this.storage.get(TODO_KEY).then((tasks: Todo[]) => {
      if (!tasks || HTMLIonItemSlidingElement.length === 0) {
        return null;
      }
      const keepTasks: Todo[] = [];
      for (const i of tasks) {
        if (i.id !== id) {
          keepTasks.push(i);
        }
      }
      return this.storage.set(TODO_KEY, keepTasks);
    });
  }
}
