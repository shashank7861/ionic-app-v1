import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TodoService } from '../services/todo.service';
import { Todo } from '../todo/todo.model';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  task: Todo;
  constructor(
    private activatedRoute: ActivatedRoute,
    private ts: TodoService,
    private router: Router,
    private alertCtrl: AlertController
    ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('id')) {
        this.router.navigate(['/todo']);
        return;
      }
      const id = paramMap.get('id');
      this.task = this.ts.gettask(id);
    });
  }
  delTask() {
    this.alertCtrl.create({
      header: 'Are you sure ?',
      message: 'Do you really want to delete this task ?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'Delete',
        handler: () => {
          this.ts.deleteTask(this.task.id);
          this.router.navigate(['/todo']);
        }
      }
      ]
    }).then(alertEl => {
      alertEl.present();
    });
  }

}
